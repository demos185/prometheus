## Demo Project:
Configure Alerting for our Application
## Technologies used:
Prometheus, Kubernetes, Linux
## Project Decription:
Configure our Monitoring Stack to notify us whenever CPU usage > 50% or Pod cannot start
* Configure Alert Rules in Prometheus Server
* Configure Alert manager with Email Receiver
## Description in details:
### Configure Alert Rules in Prometheus Server
__Step 1:__ Create 1st Rule
1. In monitoring directory create file `alert-rules.yaml`
2. Copy `AlertmanagerFailedReload` conf  from Prometheus alerts and paste it into created file
3. change name of alert on `HostHighCpuLoad`
4. change `expr` on this
```yaml
expr: 100 - (avg by(instance) (rate(node_cpu_seconds_total{mode='idle'}[2m])) * 100) >50
```
>- `node_cpu_seconds_total{mode='idle'}` - how much Cpu is idle
>- `rate ... [2m]` show a avarage rate per second over a period of two minutes
>- `(avg by(instance) .... * 100)`  shows average in persentage
>- `100 - .... >50` means "fire allert in this over 50
5. Change severity on `warning`
6. Change `description`
```yaml
description: "CPU load on host is over 50%\n Value = {{ $value }}"
```
>- `value` - shows current % of used Cpu

### Configure Alert manager with Email Receiver
>__Important__
>- Prometheus Operator extends the Kuebernetes API
>- You create custom K8s resources
>- Operator takes your custom K8s resource and tells Prometheus to reload the alert rules

__Step 1:__ Write configuration

1. Write K8s configuration inside created file which starts with API version
```yaml
api_version: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: main-rules
  namespace: monitoring
spec: 
  groups:
  - name: main.rules
    rules: 
    - alert:HostHighCpuLoad
```
>- `spec` information for this config you can find in documentation monitoring.coreos.com/v1
2. Now you can add remaining code to your config like this:
```yaml
api_version: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: main-rules
  namespace: monitoring
spec: 
  groups:
  - name: main.rules
    rules: 
    - alert: HostHighCpuLoad
      expr: 100 - (avg by(instance) (rate(node_cpu_seconds_total{mode="idle"}[2m])) * 100) > 50
      for: 2m
      labels:
        severity: warning
        namespace: monitoring
      annotations:
        description: "CPU load on host is over 50%\n Value = {{ $value }}\n Instance = {{ $labels.instance }}\n"
        summary: "Host CPU load high"
```
__Step 2:__ Write 2nd alert rule

>__Note:__ You can write multiple rule in 1 configuration
1. Add rule
```yaml
    - alert: KubernetesPodCrashLooping
      expr: kube_pod_container_status_restarts_total > 5
      for: 0m
      labels:
        severity: critical
        namespace: monitoring
      annotations: 
        description: "Pod {{ $labels.pod }} is crash looping\n Value = {{ $value }}"
        summary: "Kubernetes pod crash looping"
```
> `expr` - if container can't restarts 5 times 
> `description` - container is looping 

2. Add label to configuration

>__Note 1:__ For the Prometheus operator to automatically pick up new rules, you need to add some labels

```yaml
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: main-rules
  namespace: monitoring
  labels:
    app: kube-prometheus-stack 
    release: monitoring
```
>__Note 2:__ One of the main usages of labels is to identify or to label some of the resources so that other K8s resources can match those labels

__Step 3:__ Apply Rules
1. APlly created file 
```yaml
kubectl apply -f alert-rules.yaml
``` 
