## List of Demos

__1.__ Install Prometheus Stack in Kubernetes

__2.__ Configure Alerting for our Application

__3.__ Configure Monitoring for a Third-Party Application

__4.__ Configure Monitoring for Own Application

## Files, Folders and links

[online-shop](https://github.com/nanuchi/microservices-demo) - Demo1

