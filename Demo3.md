## Demo Project:
Configure Monitoring for a Third-Party Application
## Technologies used:
Prometheus, Kubernetes, Redis, Helm, Grafana
## Project Decription:
Monitor Redis by using Prometheus Exporter
* Deploy Redis service in our cluster
* Deploy Redis exporter using Helm Chart
* Configure Alert Rules (when Redis is down or has too many connections)
* Import Grafana Dashboard for Redis to visualize monitoring data in Grafana
## Description in details:
_Note_ In this demo used a cluster from previous demos
### Deploy Redis service in our cluster

### Deploy Redis exporter using Helm Chart
__Step 1:__ Configure values (you can see configurations in docs and inside values file in [helm chart](https://github.com/prometheus-community/helm-charts/tree/main/charts/prometheus-redis-exporter))
1. Create `values.yaml` file
2. Enable ServiceMonitor
```yaml
serviceMonitor:
  enabled: true
```
>__ServiceMonitor__
>- Describes the set of targets to be monitored by Prometheus

3. Add label and address
```yaml
serviceMonitor:
  enabled: true
labels:
  relaese: monitoring

redisAddress: redis://redis-cart:6379
```
__Step 2:__ Add and install Helm chart
1. Add Helm chart
```sh
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```
2. Install
```sh
helm install [RELEASE_NAME] prometheus-community/prometheus-redis-exporter -f redis-values.yaml
```
> Change `[RELEASE_NAME]` on redis-exporter and add created file for valuse

### Configure Alert Rules (when Redis is down or has too many connections)
__Step 1:__ Write configuration
1. Create prometheus rule for metrics (create file `alert-rules.yaml`)
```yaml
apiVersion: monitoring.coreos.com/v1
kind: PrometheusRule
metadata:
  name: redis-rules
  labels:
    app: kube-prometheus-stack 
    release: monitoring
spec:
  groups:
  - name: redis.rules
    rules:
    - alert: RedisDown
      expr: redis_up == 0
      for: 0m
      labels:
        severity: critical
      annotations:
        summary: Redis down (instance {{ $labels.instance }})
        description: "Redis instance is down\n  VALUE = {{ $value }}\n  LABELS = {{ $labels }}"
    - alert: RedisTooManyConnections
      expr: redis_connected_clients > 100
      for: 2m
      labels:
        severity: warning
      annotations:
        summary: Redis too many connections (instance {{ $labels.instance }})
        description: "Redis instance has  {{ $value }} connections\n  LABELS = {{ $labels }}"

```
>- `rules:` you can find complete rules [here](https://awesome-prometheus-alerts.grep.to/)
2. Apply rules
```sh
kubectl apply -f redis-rules.yaml
```
### Import Grafana Dashboard for Redis to visualize monitoring data in Grafana
__Step 1:__ Create Redis Dashboard in Grafana
1. You can create existing dashboards using resources like [this](https://grafana.com/grafana/dashboards/) or google. __IMPORTANT thing__ that you need to make sure that it is using correct metrics
2. Import Dashbort
  * Open Grafana Dashboard
  * Open `Create/import`
  * `import via grafana.com` - put here ID of dashboard
  * Load
  * Configure a dashboard
  * Import