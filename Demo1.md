## Demo Project:
Install Prometheus Stack in Kubernetes
## Technologies used:
Prometheus, Kubernetes, Helm, AWS EKS, eksctl, Grafana, Linux
## Project Decription:
* Setup EKS cluster using eksctl
* Deploy Prometheus, Alert Manager and Grafana in cluster as part of the Prometheus Operator using Helm chart
## Description in details:
### Setup EKS cluster using eksctl
__Step 1:__ Create cluster
```sh
eksctl create cluster
```
__Note:__ This command will create cluster with default region, default AWS credentials, 2 Worker Nodes

### Deploy Prometheus, Alert Manager and Grafana in cluster as part of the Prometheus Operator using Helm chart
__Step 1:__ Deploy Microservices Application(this config from kubernetes demo)
```sh
kubectl apply -f config-microservices.yaml
```
__Step 2:__ Deploy Prometheus Stack using Helm ([helm chart](https://github.com/prometheus-community/helm-charts))
1. Add repo
```sh
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
```
2. Update
```sh
helm repo update
```
3. Install Prometheus into __own namespace__ (Create namespace using `kubectl create namespace`)
```sh
kubectl create name space monitoring
```
4. Install chart
```sh
helm install monitoring prometheus-community/kube-prometheus-stack -n monitoring
```
---
__Understanding Prometheus Stack Components__
Components:
- 2 StatefulSets (Prometheus itself, Alertmanager)
- 3 Deployments (Prometheus, Grafana, Kube State Metrics)
- 3 ReplicaSets 
- 1 DeamonSet (Node Exporter DaemonSet)
  - Daemonset __runs on every worker Node__
  - __Node Exporter  DaemonSet__ connects to Server, traslates Worker Node metrics to Prometheus metrics
- ConfigMaps
  - configurations for different parts
  - Managed by Operator
  - How to connect to defaylt metrics
- Secrets
  -  for Grafana
  -  for Prometheus
  -  for Operator
  -  for Alerts
- Pods
- Services (every Pod have own)
- CRDs (extension of k8s API)
  - custom resource definitions 