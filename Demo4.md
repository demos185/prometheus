## Demo Project:
Configure Monitoring for Own Application
## Technologies used:
Prometheus, Kubernetes, Node.js, Grafana, Docker, DockerHub
## Project Decription:
* Configure our NodeJS application to collect & expose Metrics with Prometheus Client Library
* Deploy the NodeJS application, which has a metrics endpoint configured, into Kubernetes cluster
* Configure Prometheus to scrape this exposed metrics and visualize it in Grafana Dashboard
## Description in details:
### Configure our NodeJS application to collect & expose Metrics with Prometheus Client Library
### Deploy the NodeJS application, which has a metrics endpoint configured, into Kubernetes cluster
__Step 1:__ Build Docker image and push to Private docker repository
__Step 2:__ Deploy App into K8s cluster
1. Create deploy and service for app (`k8s-config.yaml`)
2. Paste basic configuration and adjust it
3. Create Secret
```sh
bukectl create secret docker-registry my-registry-key --docker-server= https://index.docker.io/v1/ --docker-username=name_here --docker-password= password here
```
4. In deployment add your secret

### Configure Prometheus to scrape this exposed metrics and visualize it in Grafana Dashboard
__Step 1:__ Create ServiceMonitor
1. Create component in the file where deployment and service
```yaml
---
apiVersion: monitoring.coreos.com/v1
kind: ServiceMonitor
metadata:
  name: monitoring-node-app
  labels:
    release: monitoring
    app: nodeapp
spec: 
  endpoints:
  - path: /metrics
    port: service
    targerPort: 3000
  namespaceSelector:
    matchNames:
    - default
  selector:
    matchLabels:
      app: node-app
```
>- `port: service` - We named port in service block
>```yaml
>ports:
>- name: servie
>  protocol: TCP
>  port: 3000
>  targetPort: 3000
>```
>- `namespaceSelector` - here you configure namespacec(in this configuration nodejs-app have `default` namespace and prometheus server have `monitoring` namespace)

2. Apply config
```sh
kubectl apply -f k8s-config.yaml 
```
3. Create new Dashboard in grafana with new Scrape